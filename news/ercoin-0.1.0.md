---
title: Ercoin 0.1.0
date: 2019-03-28
---
Ercoin 0.1.0 has been released. The [test network](https://testnet.ercoin.tech) is scheduled to launch at 6 P.M. UTC on Friday, 29 March.

As the version number suggests, this is not a stable release, which means that we are not in production yet. The process of achieving version 1.0 and starting the main network will consist of several areas of activity, including:

* finding bugs on the test network and improving the code;
* working towards adoption by exchanges;
* improving documentation;
* marketing.

When a satisfactory level in these areas is achieved, it is planned to announce Initial Burn Offering (IBO) — initial set of ercoins is going to be distributed proportionally to the amount of [blackcoins](https://blackcoin.org) burnt in some time window.

To learn about design goals and features, read the [whitepaper](/whitepaper.html).

*Update (29 March):*

Version 0.1.1 has been released which updates the entropy library. Launch of the test network will happen using a newly-generated genesis data which uses real entropy instead of the dummy one.
